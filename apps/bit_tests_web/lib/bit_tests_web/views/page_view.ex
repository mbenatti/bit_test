defmodule Bit.Tests.Web.PageView do
  use Bit.Tests.Web, :view

  def render("view.json", %{images: images, conn: conn}) do
    %{
      images: Enum.map(images, &image_json(&1,url(conn)))
    }
  end

  def image_json(image, url) do
    %{
      base: url<>image.base_path,
      small: url<>image.small_path,
      medium: url<>image.medium_path,
      large: url<>image.large_path
    }
  end
end
