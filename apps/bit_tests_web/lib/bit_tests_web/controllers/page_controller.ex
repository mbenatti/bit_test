defmodule Bit.Tests.Web.PageController do
  use Bit.Tests.Web, :controller

  alias Bit.Tests.Repo
  alias Mogrify


  @doc """
    {
        "images": [
            {
                "url": "http://54.152.221.29/images/b737_5.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b777_5.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b737_3.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b777_4.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b777_3.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b737_2.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b777_2.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b777_1.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b737_4.jpg"
            },
            {
                "url": "http://54.152.221.29/images/b737_1.jpg"
            }
        ]
    }
  """
  @endpoint "http://54.152.221.29/images.json"

  def index(conn, _params) do
    images = Bit.Tests.Images
    |> Repo.all

    render conn, "index.html", images: images
  end

  def get_images(conn, _params) do
    images = Bit.Tests.Images
    |> Repo.all

    if Enum.empty?(images) do
      response = Tesla.get(@endpoint).body
      |> Poison.decode!

      Enum.each(Enum.with_index(response["images"]), fn {%{"url" => url}, index} ->
        img_binary = Tesla.get(url).body

        url_path = "/images/bit/"<>Integer.to_string(index)<>"/"
        static_dir = "./priv/static"
        img_base_path = url_path <> "base.jpg"
        img_small_path = url_path <> "small.jpg"
        img_medium_path = url_path <> "medium.jpg"
        img_large_path = url_path <> "larger.jpg"

        File.mkdir_p(static_dir<>url_path) #Create dir if not exist
        File.write!(static_dir<>img_base_path, img_binary) #write the base image to disk

        #Write small(320x240).
        Mogrify.open(static_dir<>img_base_path)
        |> Mogrify.resize("320x240")
        |> Mogrify.save(path: static_dir<>img_small_path)

        #Write medium (384x288).
        Mogrify.open(static_dir<>img_base_path)
        |> Mogrify.resize("384x288")
        |> Mogrify.save(path: static_dir<>img_medium_path)

        #Write large (640x480).
        Mogrify.open(static_dir<>img_base_path)
        |> Mogrify.resize("640x480")
        |> Mogrify.save(path: static_dir<>img_large_path)

        #Insert at mongoDB
        %Bit.Tests.Images{base: img_binary, base_path: img_base_path, small_path: img_small_path,
        medium_path: img_medium_path, large_path: img_large_path }
        |> Repo.insert

      end)

      redirect(conn ,to: "/")
    else
      put_flash(conn, :error, "Imagens já carregadas!")
      |> redirect(to: "/")
    end
  end

  def view(conn, _params) do
    images = Bit.Tests.Images
    |> Repo.all

    render conn, "view.json", images: images
  end

  def remove_all(conn, _params) do
    Bit.Tests.Images
    |> Repo.delete_all

    File.rm_rf("./priv/static/images/bit/")

    redirect(conn ,to: "/")
  end
end
