defmodule Bit.Tests.Web.Router do
  use Bit.Tests.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Bit.Tests.Web do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/get_images", PageController, :get_images
    get "/remove_all", PageController, :remove_all
    get "/view", PageController, :view
  end

  # Other scopes may use custom stacks.
  # scope "/api", Bit.Tests.Web do
  #   pipe_through :api
  # end
end
