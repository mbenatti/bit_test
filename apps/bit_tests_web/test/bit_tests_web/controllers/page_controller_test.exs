defmodule Bit.Tests.Web.PageControllerTest do
  use Bit.Tests.Web.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to Phoenix!"
  end
end
