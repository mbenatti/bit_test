# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bit_tests_web,
  namespace: Bit.Tests.Web,
  ecto_repos: [Bit.Tests.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :bit_tests_web, Bit.Tests.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "+u0dmAJbOgfTstJYz3BkALULLT+kORZjAOvPx6JmkPqeUim3XWzzPJJYMRKbMZe1",
  render_errors: [view: Bit.Tests.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Bit.Tests.Web.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :bit_tests_web, :generators,
  context_app: :bit_tests

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
