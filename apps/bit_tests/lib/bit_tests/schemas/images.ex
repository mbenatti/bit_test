defmodule Bit.Tests.Images do
  use Ecto.Schema

  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "images" do
    field :base, :binary
    field :base_path, :string
    field :small_path, :string
    field :medium_path, :string
    field :large_path, :string
  end

  @allowed [:base, :base_path, :small_path, :medium_path, :large_path]

  def changeset(images, params \\ %{}) do
    images
    |> cast(params, @allowed)
  end
end
