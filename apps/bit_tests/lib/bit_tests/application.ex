defmodule Bit.Tests.Application do
  @moduledoc """
  The Bit.Tests Application Service.

  The bit_tests system business domain lives in this application.

  Exposes API to clients such as the `Bit.Tests.Web` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link([
      supervisor(Bit.Tests.Repo, []),
    ], strategy: :one_for_one, name: Bit.Tests.Supervisor)
  end
end
