use Mix.Config

import_config "prod.secret.exs"

config :bit_tests, Bit.Tests.Repo,
  adapter: Mongo.Ecto,
  url: System.get_env("MONGODB_URI"),
  pool_size: 20
