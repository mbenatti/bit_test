use Mix.Config

config :bit_tests, ecto_repos: [Bit.Tests.Repo]

import_config "#{Mix.env}.exs"
