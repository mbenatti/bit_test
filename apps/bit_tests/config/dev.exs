use Mix.Config

config :bit_tests, Bit.Tests.Repo,
  adapter: Mongo.Ecto,
  database: "bit_tests_dev",
  pool_size: 10

