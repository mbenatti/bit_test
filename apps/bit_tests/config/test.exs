use Mix.Config

config :bit_tests, Bit.Tests.Repo,
  adapter: Mongo.Ecto,
  database: "bit_tests_test",
  pool_size: 1
