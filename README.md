# Problem description
[ResizePhoto.pdf](https://bitbucket.org/mbenatti/bit_test/raw/f6f4f831635fc8dab22149e81bab83213371a59d/ResizePhoto.pdf)

# Requirements/Versions Used
- Elixir ~>1.4.2
- Erlang ~>1.9.3
- Phoenix Framework ~>1.3rc.1
- Mongodb ~>3.0 (without user/password to login for dev)
- imagemagick (see Dependency below)
- npm and nodejs(>5) (for static resources) 

## DEPENDENCIES (for imagemagick)

### Linux ###

#### Example: Debian/Ubuntu ####

```bash
$ sudo apt-get install imagemagick
```

##### **Note:** All of these examples was running in mint 18.2 that is based on ubuntu 16.04 #####

# How to run

```
* git clone 
* cd bit_tests_web
* mix deps.get
* mix deps.compile
* cd apps/bit_tests_web/assets && npm install && node node_modules/brunch/bin/brunch build
* cd apps/bit_tests_web && mix phx.server
```

#Notes

This project fill everything that was required in document ResizePhoto.pdf(in the root folder)

Using it, you will: 

1) Load Images(and make a resizing to three different's format's, plus save to the database)

2) Remove Images(using cache/dir local and database)

3) See/Download Imagens(that are in cache and database)

4) See Json(With loaded images)

* Has a Cache, don't load twice.
* Load the images using an external url/webservice and save to the mongodb
* The project was splited with other two projects in an "umbrella":
   1º) Web Project(Handles visualization, renderization and has a controller of the requests that resize the image in the first load)
   2º) Model/Schemas Project, Save data using mongodb (see yourpath/bit_tests_umbrella/apps/bit_tests/lib/bit_tests/schemas/images.ex)

----

The online example can't be a performative because it use a free cloud machine with 1 core and 1gb ram. 

*In the first load, The `Get Images` can spend a little bit more time to load because it will resize all images and save to the database, in a **production** ready project this process would be in background, becoming transparent to the user* 

REPO: https://bitbucket.org/mbenatti/bit_test
URL of the example: http://13.66.51.208:4000/


#Notas (Em português)

Esse projeto completa tudo que foi requerido no documento ResizePhoto.pdf(que está na raiz).

Nele você pode: 

1) Carregar Imagems(e faz o redimensionamento para três formatos, além de salvar no banco)

2) Remover Imagens(do cache/dir local e database)

3) Ver/Baixar imagens(que estão no cache e database)

4) Ver Json(das imagens carregadas)

* Possui Cache, não carrega duas vezes.
* Carrega imagens da url e salva no mongodb
* O projeto foi dividido em dois subprojetos dentro de uma "umbrella":
   1º) Projeto Web(Trata da visualização, renderização e possui um controlador das requisições e transformações de imagem)
   2º) Modelo ou Schemas, Salvo no mongodb (vide yourpath/bit_tests_umbrella/apps/bit_tests/lib/bit_tests/schemas/images.ex)


----

O Aplicativo está disponível online e poderá não ser muito performático pois usa recursos grátis com uma maquina de 1 core e 1gb...

*O `Get Images` pode demorar um pouco pois além de carregar o json ele já redimensiona todas as imagens e salva no banco de dados,
em ambiente de produção real este processamento seria feito em background, transparente para o usuário* 

REPO: https://bitbucket.org/mbenatti/bit_test
URL: http://13.66.51.208:4000/